/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Construct} from 'constructs';
import {aws_iam as iam, aws_s3 as s3, aws_events as events,aws_events_targets as targets, Duration} from "aws-cdk-lib";
import {DockerImageCode, DockerImageFunction} from "aws-cdk-lib/aws-lambda";
import * as path from "path";
import {createYYYYMMDD} from "./utils";

export interface ServerlessS3ClamScanDBUpgradeLambdaProps {
    clamDbBucket: s3.IBucket
    region: string
    account: string
}

/**
 * @author 彭程
 */
export class ServerlessS3ClamScanDBUpgradeLambda extends Construct {

    constructor(scope: Construct, id: string, props: ServerlessS3ClamScanDBUpgradeLambdaProps) {
        super(scope, id);

        const dbUpgradePolicy = new iam.Policy(this, 'policy-serverless-s3-clam-scan-db-upgrade', {
            policyName: "serverless-s3-clam-scan-db-upgrade-policy",
            statements: [new iam.PolicyStatement({
                effect: iam.Effect.ALLOW,
                actions: [
                    "s3:PutObject",
                    "s3:GetObject",
                    "s3:HeadObject",
                    "s3:DeleteObject",
                    "s3:DeleteObjects",
                    "ssm:GetParametersByPath",
                    "ssm:PutParameter",
                    "ssm:GetParameterHistory",
                    "ssm:GetParametersByPath",
                    "ssm:GetParameters",
                ],
                resources: [
                    `arn:aws:s3:::${props.clamDbBucket.bucketName}/*`,
                    `arn:aws:ssm:${props.region}:${props.account}:parameter/serverless-s3-clam-scan*`,
                ],
            }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "s3:ListBucket",
                        "s3:HeadBucket",
                    ],
                    resources: [
                        `arn:aws:s3:::${props.clamDbBucket.bucketName}`
                    ],
                }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "cloudwatch:PutMetricData",
                        "logs:CreateLogGroup",
                        "logs:CreateLogStream",
                        "logs:PutLogEvents"
                    ],
                    resources: [
                        `*`
                    ],
                })
            ],
        });

        const dbUpgradeLambdaRole = new iam.Role(this, 'role-serverless-s3-clam-scan-db-upgrade', {
            roleName: "serverless-s3-clam-scan-db-upgrade-role",
            assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com')
        })
        dbUpgradePolicy.attachToRole(dbUpgradeLambdaRole);

        // Define lambda function
        const dbUpgradeLambda = new DockerImageFunction(this, 'ServerlessS3ClamScanDbUpgrade', {
            functionName: "ServerlessS3ClamScanDbUpgrade",
            code: DockerImageCode.fromImageAsset(
                path.join(__dirname, '../src/db_upgrade'),
                {
                    buildArgs: {
                        CACHE_DATE:createYYYYMMDD()
                    }
                }
            ),
            timeout: Duration.seconds(100),
            memorySize: 2048,
            role: dbUpgradeLambdaRole,
            environment:{
                CLAMAV_DATABASE_BUCKET_NAME:props.clamDbBucket.bucketName
            }
        });

        // call update clamav database lambda every 4 hours
        const rule = new events.Rule(this, 'event-rule-ServerlessS3ClamScanDbUpgrade', {
            schedule: events.Schedule.rate(Duration.hours(4)),
            targets: [new targets.LambdaFunction(dbUpgradeLambda)],
        });
    }
}