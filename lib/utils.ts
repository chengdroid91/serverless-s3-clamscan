export const createYYYYMMDD = () => {
    const today = new Date();
    const monthMM = ('0' + (today.getMonth() + 1)).slice(-2);
    const dayDD = ('0' + today.getDate()).slice(-2);
    return today.getFullYear().toString() + monthMM + dayDD;
};