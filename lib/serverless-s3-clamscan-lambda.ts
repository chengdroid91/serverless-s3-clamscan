/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Construct} from 'constructs';
import {aws_iam as iam, aws_sqs as sqs, aws_s3 as s3, aws_s3_notifications as s3_notify, Duration} from "aws-cdk-lib";
import {DockerImageCode, DockerImageFunction} from "aws-cdk-lib/aws-lambda";
import {SqsEventSource} from "aws-cdk-lib/aws-lambda-event-sources";
import * as path from "path";
import {createYYYYMMDD} from "./utils";

export interface ServerlessS3ClamScanLambdaProps {
    scanBucketArray: Array<s3.IBucket>
    clamDbBucket: s3.IBucket
    region: string
    account: string
}

const LAMBDA_EXECUTION_TIMEOUT = 200
const MAX_BATCHING_WINDOW = 20
// https://zaccharles.medium.com/lambda-concurrency-limits-and-sqs-triggers-dont-mix-well-sometimes-eb23d90122e0
const SQS_VISIBILITY_TIMEOUT = 30 * 6 + MAX_BATCHING_WINDOW + 5;

/**
 * @author 彭程
 */
export class ServerlessS3ClamScanLambda extends Construct {

    constructor(scope: Construct, id: string, props: ServerlessS3ClamScanLambdaProps) {
        super(scope, id);

        const scanBucketObjectArnArray = props.scanBucketArray.map(scanBucket => `arn:aws:s3:::${scanBucket.bucketName}/*`)
        const scanBucketArnArray = props.scanBucketArray.map(scanBucket => `arn:aws:s3:::${scanBucket.bucketName}`)

        const scanPolicy = new iam.Policy(this, 'policy-serverless-s3-clam-scan', {
            policyName: "serverless-s3-clam-scan-policy",
            statements: [
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "s3:ListBucket",
                        "s3:HeadBucket"
                    ],
                    resources: [
                        ...scanBucketArnArray
                    ],
                }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "s3:PutObject",
                        "s3:GetObject",
                        "s3:HeadObject",
                        "s3:DeleteObject",
                        "s3:DeleteObjects",
                    ],
                    resources: [
                        ...scanBucketObjectArnArray
                    ],
                }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "ssm:GetParametersByPath",
                        "ssm:GetParameterHistory",
                        "ssm:GetParametersByPath",
                        "ssm:GetParameters",
                        "ssm:GetParameter"
                    ],
                    resources: [
                        `arn:aws:ssm:${props.region}:${props.account}:parameter/serverless-s3-clam-scan*`,
                    ],
                }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "s3:GetObject",
                        "s3:ListBucket"
                    ],
                    resources: [
                        `arn:aws:s3:::${props.clamDbBucket.bucketName}/*`,
                        `arn:aws:s3:::${props.clamDbBucket.bucketName}`,
                    ],
                }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "cloudwatch:PutMetricData",
                        "logs:CreateLogGroup",
                        "logs:CreateLogStream",
                        "logs:PutLogEvents"
                    ],
                    resources: [
                        `*`
                    ],
                })
            ],
        });

        const queue = new sqs.Queue(this, 'queue-serverless-s3-clam-scan', {
            queueName: 'serverless-s3-clam-scan-queue',
            visibilityTimeout: Duration.seconds(SQS_VISIBILITY_TIMEOUT),
            retentionPeriod: Duration.days(14)
        });

        // Define the role for lambda
        const scanLambdaRole = new iam.Role(this, 'role-serverless-s3-clam-scan', {
            roleName: "serverless-s3-clam-scan-lambda-role",
            assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com')
        })
        scanPolicy.attachToRole(scanLambdaRole);

        // Define lambda function
        const scanLambda = new DockerImageFunction(this, 'ServerlessS3ClamScan', {
            functionName: "ServerlessS3ClamScan",
            code: DockerImageCode.fromImageAsset(
                path.join(__dirname, '../src/scan'),
                {
                    buildArgs: {
                        CACHE_DATE:createYYYYMMDD()
                    }
                }
            ),
            timeout: Duration.seconds(LAMBDA_EXECUTION_TIMEOUT),
            reservedConcurrentExecutions: 30,
            memorySize: 2048,
            role: scanLambdaRole,
            environment: {
                CLAMAV_DATABASE_BUCKET_NAME: props.clamDbBucket.bucketName
            }
        });

        // Add sqs event source to sqs
        scanLambda.addEventSource(new SqsEventSource(queue, {
            batchSize: 10,
            maxBatchingWindow: Duration.seconds(MAX_BATCHING_WINDOW)
        }))

        // Create s3 object create notification to sqs
        for (let scanBucket of props.scanBucketArray) {
            const s3Notify = new s3_notify.SqsDestination(queue);
            s3Notify.bind(this, scanBucket);
            scanBucket.addObjectCreatedNotification(s3Notify);
        }
    }
}