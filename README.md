# Serverless S3 Object Virus Scan

无服务s3文件自动病毒检测

## AWS构成图
![alt text](aws-architecture.png)


## 处理流程
```
1,2   更新病毒数据lambda每四个小时执行一次，会把最新的病毒数据库下载下来存放到s3上，并将病毒库版本更新到parameter store中
3     每当文件上传到要扫描的bucket中时，将改文件的object create事件发送到sqs中
4     sqs会触发扫描文件lambda,一次lambda的执行会检查不超过10个文件，只扫描小于20M大小的文件
5,6   扫描文件lambda会使用parameter store中的病毒库版本号从s3下载病毒库数据来扫描文件，扫描出的被感染的文件会从s3删除
7     扫描出的病毒文件数量会加入到cloudwatch matric中
7,8   lambda执行日志会发送到cloudwatch log中
9,10  lambda是从ecr拉取镜像执行的
```

## aws构筑手顺
### 需要预先安装以下工具
* docker
* nodejs
* aws cli v2

### 部署命令
```
# 安装cdk
npm install -g aws-cdk

# 安装依赖
npm install

# 初期化cdk
cdk bootstrap aws://{aws账户id}/{区域}

# 部署
export SCAN_BUCKET_NAME_LIST={需要扫描的bucket名列表} \
&& export CLAMAV_DATABASE_BUCKET_NAME={存放clamav病毒数据的bucket名} \
&& cdk deploy
```

## 查看扫描日志
```
aws logs tail --follow /aws/lambda/ServerlessS3ClamScan
```

## 查看检测出的感染文件数指标
```
aws cloudwatch get-metric-statistics \
    --metric-name INFECTED \
    --start-time 2022-02-20T02:35:00Z \
    --end-time 2022-02-22T23:18:00Z \
    --period 3600 \
    --namespace "serverless-s3-clam-scan" \
    --statistics Sum \
    --dimensions Name=service,Value=scan
```


## License

The project is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).
