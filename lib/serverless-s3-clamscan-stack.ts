/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {aws_s3 as s3, aws_ssm as ssm, Duration, Stack, StackProps} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {ServerlessS3ClamScanLambda} from "./serverless-s3-clamscan-lambda";
import {ServerlessS3ClamScanDBUpgradeLambda} from "./serverless-s3-clamscan-db-upgrade-lambda";

/**
 * @author 彭程
 */
export class ServerlessS3ClamScanStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        if (!process.env.SCAN_BUCKET_NAME_LIST) {
            throw "please specify valid bucket name list to scan."
        }

        if (!process.env.CLAMAV_DATABASE_BUCKET_NAME) {
            throw "please specify valid bucket name to storage clamav database file."
        }

        const scanBucketNameArray = process.env.SCAN_BUCKET_NAME_LIST?.split(",");

        if (!scanBucketNameArray || scanBucketNameArray.length === 0) {
            throw "please specify valid bucket name to scan."
        }

        const scanBucketArray = scanBucketNameArray.map(scanBucketName => {
            return s3.Bucket.fromBucketName(this, `id-${scanBucketName}`, scanBucketName);
        })

        const clamavDatabaseBucket = new s3.Bucket(this, 's3-serverless-s3-clam-scan-db', {
            bucketName: process.env.CLAMAV_DATABASE_BUCKET_NAME,
            lifecycleRules: [
                {
                    expiration: Duration.days(1) // delete file after one day
                }
            ]
        })

        const clamavDatabaseVersionParameter = new ssm.StringParameter(this, 'id-clamav-database-version', {
            parameterName: '/serverless-s3-clam-scan/db/version',
            stringValue: '0',
            description: 'clamav database version',
            type: ssm.ParameterType.STRING,
        });

        new ServerlessS3ClamScanLambda(this, "ServerlessS3ClamScanLambda", {
            scanBucketArray: scanBucketArray,
            clamDbBucket: clamavDatabaseBucket,
            region: this.region,
            account: this.account
        })

        new ServerlessS3ClamScanDBUpgradeLambda(this, "ServerlessS3ClamScanDbUpgradeLambda", {
            clamDbBucket: clamavDatabaseBucket,
            region: this.region,
            account: this.account
        })
    }
}
