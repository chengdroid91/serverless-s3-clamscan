"""
Copyright 2002-2022 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
ou may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import json
import os
import shutil
import subprocess
from enum import Enum
from urllib.parse import unquote_plus

import boto3
from aws_lambda_powertools import Logger, Metrics
from aws_lambda_powertools.metrics import MetricUnit
from botocore.exceptions import ClientError

logger = Logger()
metrics = Metrics(namespace="serverless-s3-clam-scan", service="scan")

s3_resource = boto3.resource("s3")
s3_client = boto3.client("s3")
ssm_client = boto3.client('ssm')

MAX_FILE_SIZE_BYTES = 1024 * 1024 * 20


class ScanStatus(Enum):
    OK = 1
    NG = 2
    UN_KNOW = 3


class ClamAVException(Exception):
    """Raise when ClamAV returns an unexpected exit code"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return str(self.message)


@logger.inject_lambda_context(log_event=True)
@metrics.log_metrics
def handler(event, context):
    file_download_folder = f"/tmp/{context.aws_request_id}"
    s3_file_download_folder = f"{file_download_folder}/s3"
    scan_temp_folder = f"{file_download_folder}/scan_temp"
    clamav_database_folder = f"{file_download_folder}/database"

    if not event.get('Records'):
        return {
            'result': 'success'
        }

    version = _get_db_version_from_parameter_store()
    logger.info(f"Fetched clamav database version is {version}")

    try:
        _create_dir(file_download_folder)
        _create_dir(scan_temp_folder)
        _create_dir(s3_file_download_folder)

        # download clamav database file from s3 to local by version
        if version:
            _create_dir(clamav_database_folder)
            _download_file_from_s3_folder(os.getenv("CLAMAV_DATABASE_BUCKET_NAME"), version, clamav_database_folder)

        scan_s3_objects = []

        for record in event['Records']:
            if not record.get("body"):
                continue

            s3_event = json.loads(record["body"])

            if not s3_event.get('Records'):
                continue

            for s3_event_record in s3_event['Records']:
                bucket = s3_event_record['s3']['bucket']['name']
                key = s3_event_record['s3']['object']['key']

                bucket_s3_file_download_folder = f"{s3_file_download_folder}/{bucket}"
                _create_dir(bucket_s3_file_download_folder)

                file_download_path = f"{bucket_s3_file_download_folder}/{key}"

                try:
                    # only scan the file that size less than 20M
                    content_size = s3_resource.Object(bucket, unquote_plus(key)).content_length
                    if content_size > MAX_FILE_SIZE_BYTES:
                        logger.warning(f's3://{bucket}/{unquote_plus(key)}: The file size larger than 20M.'
                                       f'Will not scan it')
                        continue

                    _download_object_from_s3(bucket, key, file_download_path)
                    scan_s3_objects.append({
                        'bucket': bucket,
                        'key': key,
                        'file_download_path': file_download_path
                    })
                except ClientError as error:
                    if error.response["Error"]["Code"] == '404':
                        logger.warning(f's3://{bucket}/{unquote_plus(key)}:File not found from s3.May be deleted.')
                    else:
                        raise error

        if not scan_s3_objects:
            return {
                'result': 'success'
            }

        # scan all download files from s3.
        if _scan(s3_file_download_folder, scan_temp_folder,
                 clamav_database_folder if version else None) == ScanStatus.NG:

            infected_count = 0
            cleared_count = 0

            for scan_s3_object in scan_s3_objects:
                if os.path.exists(scan_s3_object['file_download_path']):
                    cleared_count += 1
                else:
                    infected_count += 1
                    s3_resource.Object(scan_s3_object['bucket'], unquote_plus(scan_s3_object['key'])).delete()

            metrics.add_metric(name="INFECTED", unit=MetricUnit.Count, value=infected_count)
            metrics.add_metric(name="CLEAR", unit=MetricUnit.Count, value=cleared_count)

        else:
            metrics.add_metric(name="CLEAR", unit=MetricUnit.Count, value=len(scan_s3_objects))

    finally:
        _delete_folder(file_download_folder)

    return {
        'result': 'success'
    }


def _download_file_from_s3_folder(bucket_name, prefix, download_dir):
    # download file from s3 folder
    bucket = s3_resource.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=prefix):
        download_path = f"{download_dir}/{os.path.basename(obj.key)}"
        logger.info(f"download file from s3://{bucket_name}/{obj.key} to {download_path}")
        bucket.download_file(obj.key, download_path)


def _get_db_version_from_parameter_store():
    # get clamav data version from parameter store
    try:
        response = ssm_client.get_parameter(
            Name='/serverless-s3-clam-scan/db/version',
            WithDecryption=False
        )

        if not response.get('Parameter'):
            return None

        version = response['Parameter']['Value']
        return None if version == '0' else version

    except ClientError as e:
        if e.response['Error']['Code'] == 'ParameterNotFound':
            return None
        raise e


def _create_dir(folder_path):
    """create the specified folder"""
    if not os.path.exists(folder_path):
        os.makedirs(folder_path, exist_ok=True)


def _delete_folder(folder_path):
    """delete the specified folder"""

    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)


def _download_object_from_s3(bucket, key, download_path):
    """Downloads the specified file from S3 to tmp"""
    logger.info(f"download file from s3://{bucket}/{unquote_plus(key)} to {download_path}")

    os.makedirs(os.path.dirname(download_path), exist_ok=True)
    s3_resource.Bucket(bucket).download_file(
        unquote_plus(key), download_path
    )


def _scan(scan_folder, temp_dir, clamav_database_folder=None):
    """Scans the files under the scan folder"""

    command = [
        "clamscan",
        "--remove",
        "--recursive",
        "--stdout",
        f"--tempdir={temp_dir}",
        scan_folder,
    ] if not clamav_database_folder else [
        "clamscan",
        "--remove",
        "--recursive",
        "--stdout",
        f"--database={clamav_database_folder}",
        f"--tempdir={temp_dir}",
        scan_folder,
    ]

    scan_summary = subprocess.run(
        command,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
    )

    logger.info(f"scan summary:{scan_summary}")

    if scan_summary.returncode == 0:
        return ScanStatus.OK
    elif scan_summary.returncode == 1:
        return ScanStatus.NG
    else:
        raise ClamAVException(
            f"ClamAV exited with unexpected code: {scan_summary.returncode}."
        )
