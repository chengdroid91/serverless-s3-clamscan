"""
Copyright 2002-2022 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
ou may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import datetime
import os
import shutil
import subprocess

import boto3
from aws_lambda_powertools import Logger, Metrics

logger = Logger()

s3_resource = boto3.resource("s3")
s3_client = boto3.client("s3")
ssm_client = boto3.client('ssm')


class ClamAVException(Exception):
    """Raise when ClamAV returns an unexpected exit code"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return str(self.message)


@logger.inject_lambda_context(log_event=True)
def handler(event, context):
    version = _get_now_str()
    output_dir = f"/tmp/{version}"

    _create_dir(output_dir)

    try:
        _fresh_clam_database(output_dir)

        _upload_dir_to_s3(os.getenv("CLAMAV_DATABASE_BUCKET_NAME"), version, output_dir)

        _put_version_parameter(version)

    finally:
        _delete_folder(output_dir)

    return {
        'result': 'success'
    }


def _create_dir(folder_path):
    """create the specified folder"""
    if not os.path.exists(folder_path):
        os.makedirs(folder_path, exist_ok=True)


def _delete_folder(folder_path):
    """delete the specified folder"""
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)


def _get_now_str():
    # get current time by "yyyyMMddHHmmss" type
    return datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S')


def _upload_dir_to_s3(bucket, upload_path, local_dir):
    # upload files in specified dir to s3
    for root, dirs, files in os.walk(local_dir):
        for filename in files:
            # full local path
            local_path = os.path.join(root, filename)

            relative_path = os.path.relpath(local_path, local_dir)

            s3_path = os.path.join(upload_path, relative_path)

            logger.info(f"upload {local_path} to s3://{bucket}/{s3_path}")
            s3_client.upload_file(local_path, bucket, s3_path)


def _put_version_parameter(version):
    # save version to parameter store
    result = ssm_client.put_parameter(
        Name='/serverless-s3-clam-scan/db/version',
        Value=version,
        Type='String',
        Overwrite=True
    )


def _fresh_clam_database(output_dir):
    # generate clamav database to output dir
    logger.info(f"start to generate clamav data to {output_dir}")
    command = [
        "freshclam",
        "--stdout",
        f"--datadir={output_dir}",
    ]
    update_summary = subprocess.run(
        command,
        stderr=subprocess.STDOUT,
        stdout=subprocess.PIPE,
    )
    logger.info(f"update summary:{update_summary}")

    if update_summary.returncode != 0:
        raise ClamAVException(
            f"FreshClam exited with unexpected code: {update_summary.returncode}"
        )
